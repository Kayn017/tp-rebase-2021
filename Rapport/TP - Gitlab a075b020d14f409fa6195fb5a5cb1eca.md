# TP - Gitlab

# Démarrage

J’ai commencé par créer un fork du repo tp-rebase-2021.

![Untitled](TP%20-%20Gitlab%20a075b020d14f409fa6195fb5a5cb1eca/Untitled.png)

Le repo du fork est accessible [ici](https://gitlab.com/Kayn017/tp-rebase-2021)

J’ai ensuite ajouter un remote `personal` pointant vers le nouveau repo

```bash
tanguy@portable ~/S/C/C/A/O/T/R/tp-rebase-2021 (main)> git remote -v
origin  https://gitlab.com/fnuttens/tp-rebase-2021.git (fetch)
origin  https://gitlab.com/fnuttens/tp-rebase-2021.git (push)
tanguy@portable ~/S/C/C/A/O/T/R/tp-rebase-2021 (main)> git remote add personal https://gitlab.com/Kayn017/tp-rebase-2021.git
tanguy@portable ~/S/C/C/A/O/T/R/tp-rebase-2021 (main)> git remote -v 
origin  https://gitlab.com/fnuttens/tp-rebase-2021.git (fetch)
origin  https://gitlab.com/fnuttens/tp-rebase-2021.git (push)
personal        https://gitlab.com/Kayn017/tp-rebase-2021.git (fetch)
personal        https://gitlab.com/Kayn017/tp-rebase-2021.git (push)
```

Pour vérifier le fonctionnement de cette branche, j’ai effectué la commande

```bash
git fetch personal
```

et tout fonctionnait correctement.

J’ai ensuite ajouté l’utilisateur `fnuttens` en tant que **Maintainer** à mon repo.

![Untitled](TP%20-%20Gitlab%20a075b020d14f409fa6195fb5a5cb1eca/Untitled%201.png)

# Votre branche

J’ai ensuite créé une issue trouvable [ici](https://gitlab.com/Kayn017/tp-rebase-2021/-/issues/1)

Pour résoudre cette issue, j’ai créé une branche `1-customize-readme` 

```bash
git checkout -b 1-customize-readme
```

J’ai mis à jour le fichier `README` puis j’ai commit le changement.
Au moment de push sur le repo `origin`, j’ai pu constater que git me demande un username et un mot de passe. 

Pour me connecter avec le mot de passe, j’ai généré un Personnal Access Token sur cette page

![Untitled](TP%20-%20Gitlab%20a075b020d14f409fa6195fb5a5cb1eca/Untitled%202.png)

J’ai rentré le token ainsi généré dans le champs mot de passe, mais je suis retrouvé avec une erreur 403

```bash
tanguy@portable ~/S/C/C/A/O/T/R/tp-rebase-2021 (1-customize-readme)> git push origin 1-customize-readme
Username for 'https://gitlab.com': Kayn017
Password for 'https://Kayn017@gitlab.com': 
remote: You are not allowed to upload code.
fatal : impossible d'accéder à 'https://gitlab.com/fnuttens/tp-rebase-2021.git/' : The requested URL returned error: 403
```

En effet, je n’ai pas l’autorisation d’écrire sur le repo `origin`

J’ai donc push sur mon remote `personal`

# Merge Request

J’ai créé une merge request sur gitlab et je l’ai assigné à Juliette Debressy

![Untitled](TP%20-%20Gitlab%20a075b020d14f409fa6195fb5a5cb1eca/Untitled%203.png)

Elle a ensuite mis [un commentaire sur mon code](https://gitlab.com/Kayn017/tp-rebase-2021/-/merge_requests/1#note_1162557092) auquel j’ai [répondu](https://gitlab.com/Kayn017/tp-rebase-2021/-/merge_requests/1#note_1162557670) avant qu’elle ne merge ma branche.

 Je remarque que l’issue créée précédemment s’est fermé automatiquement 

J’ai ensuite pull ma branche en local. 

# Rapport

J’ai ensuite mis ce rapport sur le repo gitlab.